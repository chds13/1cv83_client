FROM ubuntu:18.04

MAINTAINER Danila Chepurko <chds@tuta.io>

LABEL OS="Ubuntu 18.04" \
      architecture="amd64" \
      1c_version="8.3.10.2753" \
      1c_architecture="x86-64"

COPY ./dist/* /tmp/

WORKDIR /tmp/

RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections &&\
    apt-get update &&\
    apt-get install --no-install-recommends --assume-yes \
            libwebkitgtk-3.0-0 \
            ttf-mscorefonts-installer \
            libfontconfig1 \
            libgsf-1-114 \
            libglib2.0-0 \
            libodbc1 \
            libmagickwand-6.q16-3 \
            libwebkitgtk-1.0-0 \
            language-pack-ru &&\
    fc-cache -fv &&\
    dpkg -i 1c-enterprise83-common_*_amd64.deb &&\
    dpkg -i 1c-enterprise83-server_*_amd64.deb &&\
    dpkg -i 1c-enterprise83-client_*_amd64.deb &&\
    apt-get clean &&\
    rm -rf /tmp/* &&\
    useradd -m 1cer

ENV LANGUAGE=ru_RU.UTF-8
ENV LC_ALL=ru_RU.UTF-8
ENV LANG=ru_RU.UTF-8

USER 1cer

ENTRYPOINT /opt/1C/v8.3/x86_64/1cv8
