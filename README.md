### DO NOT PULL THIS IMAGE. YOU SHOULD BUILD IT IN YOUR OWN SYSTEM.

---

# Preparation
Because image from this Dockerfile doesn't have any 1c distributives inside and you can pull it from official site, you should bild it by yourself. For start it in your own system:

1. Clone this Dockerfile
2. Create ./dist/ directory
3. Put .deb 1c: **common**, **server** and **client** into ./dist/ [Download your distributes from official site](https://releases.1c.ru/)
4. Then build your own image (for example: `docker build --network=host --tag=1c:8.3.10.2753`)
5. Delete ./dist/

---
# Usage
To start container you can use:

`docker run -ti -e DISPLAY -v /mnt:/mnt -v $HOME:/home/1cer --net=host --pid=host --ipc=host *docker_image_name*`


Don't forget to issue permission to connect to X11:

`xhost local:root`

or else
